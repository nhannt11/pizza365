$(function () {
    var gID = ""
    var gOrdersArray = ""
    var gSizeCombo = [
        {kichCo: "S",duongKinh: 20, suon: 2, salad:200,soLuongNuoc: 2, donGia: 150000},
        {kichCo: "M",duongKinh: 25, suon: 4, salad:300,soLuongNuoc: 3, donGia: 200000},
        {kichCo: "L",duongKinh: 30, suon: 8, salad:500,soLuongNuoc: 4, donGia: 250000}
    ]
    const baseUrl = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    const TABLE_COLS = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"]
    const COL_ORDER_ID = 0
    const COL_SIZE = 1
    const COL_TYPE = 2
    const COL_DRINK = 3
    const COL_PRICE = 4
    const COL_NAME = 5
    const COL_PHONE = 6
    const COL_STATUS = 7
    const COL_ACTION = 8
    
    var gTable = $("table").DataTable({
        columns: [
            { data: TABLE_COLS[COL_ORDER_ID] },
            { data: TABLE_COLS[COL_SIZE] },
            { data: TABLE_COLS[COL_TYPE] },
            { data: TABLE_COLS[COL_DRINK] },
            { data: TABLE_COLS[COL_PRICE] },
            { data: TABLE_COLS[COL_NAME] },
            { data: TABLE_COLS[COL_PHONE] },
            { data: TABLE_COLS[COL_STATUS] },
            { data: TABLE_COLS[COL_ACTION] }
        ],
        columnDefs: [
            {
                targets: COL_ACTION,
                defaultContent: `<button class="btn btn-info btn-order-detail">Detail <i class="fas fa-info-circle"></i></button>
                <button class="btn btn-danger btn-order-delete">Delete <i class="fas fa-trash-alt"></i></button>`
            }
        ]
    })
    
        getAllOrdersAPI()
    
    function getAllOrdersAPI() {
        $.ajax({
            url: baseUrl,
            type: "GET",
            success: function (res) {
                gOrdersArray = res;
                console.log(gOrdersArray)
                showDataToTable(gOrdersArray)
            },
            error: function (res) {
                console.log(res)
            },
        })
    }
    $("#btn-insert").on("click",function(){
        getDrink()
        $("#inp-insert-duong-kinh").val(20)
        $("#inp-insert-suon-nuong").val(2)
        $("#inp-insert-salad").val(200)
        $("#inp-insert-number-drink").val(2)
        $("#inp-insert-prices").val(150000)

        $("#modal-insert").modal("show")
    })
    $("#select-insert-combo").on("change", function(){
        var selectedSize = $("#select-insert-combo option:selected").val()
        var selectObj = gSizeCombo.filter(data => data.kichCo == selectedSize )[0]
        //console.log(selectObj)
        $("#inp-insert-duong-kinh").val(selectObj.duongKinh)
        $("#inp-insert-suon-nuong").val(selectObj.suon)
        $("#inp-insert-salad").val(selectObj.salad)
        $("#inp-insert-number-drink").val(selectObj.soLuongNuoc)
        $("#inp-insert-prices").val(selectObj.donGia)
    })
    $("#button-insert-confirm").on("click",function(){
        var vNewOder = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        } 
        getDataFromModal(vNewOder);
        console.log(vNewOder)
        var isValid = checkValid(vNewOder)
        if (isValid) {
            $.ajax({
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8' 
                },
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
                type: "POST",
                data: JSON.stringify(vNewOder),
                success: function(res){
                    console.log(res)
                    $("#modal-insert").modal("hide")
                    getAllOrdersAPI()
                    toastr.success('Đã thêm đơn hàng thành công')
                },
                error: function(res){

                }
            })
        }
    })
    $("table").on("click", ".btn-order-detail", function () {
        //console.log(this)
        var vRow = $(this).parents("tr")
        var orderID = gTable.row(vRow).data()["orderId"]
        gID = gTable.row(vRow).data()["id"]
        console.log("order ID : " + orderID + " ---- id: " + gID)
        getOrderByOrderID(orderID)
    })
    $("table").on("click", ".btn-order-delete", function () {
        //console.log(this)
        var vRow = $(this).parents("tr")
        var orderID = gTable.row(vRow).data()["orderId"]
        gID = gTable.row(vRow).data()["id"]
        console.log("order ID : " + orderID + " ---- id: " + gID)
        $("#modal-delete").modal("show")
    })
    $("#button-delete-confirm").on("click", function(){
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/"+gID,
            type: "DELETE",
            success: function(res){
                console.log(res)
                $("#modal-delete").modal("hide")
                getAllOrdersAPI()
                toastr.info("Đã xóa thành công order")
            }
        })
    })
    $("#btn-filter-user").on("click", function () {
        var selectStatus = $("#select-status option:selected").val()
        var selectType = $("#select-type option:selected").val()
        var filterArray = gOrdersArray.filter(function (order) {
            var bStatus = ""
            var bType = ""
            if (order.trangThai != null) {
                bStatus = order.trangThai.toLowerCase()
            }
            if (order.loaiPizza != null) {
                bType = order.loaiPizza.toLowerCase()
            }
            return (bStatus.includes(selectStatus) || selectStatus == "none") && (bType.includes(selectType) || selectType == "none")
        })
        showDataToTable(filterArray)
    })
    $("#button-detail-confirm").on("click", function () {
        var vStatus = $("#select-detail-status option:selected").val()
        var object = { trangThai: vStatus }
        console.log("ID là: " + gID)
        updateOrder(object)
    })


    function showDataToTable(paramArray) {
        gTable.clear()
        gTable.rows.add(paramArray)
        gTable.draw()
    }
    function getOrderByOrderID(paramOderID) {
        $.ajax({
            url: baseUrl + `/${paramOderID}`,
            type: "GET",
            success: function (res) {
                console.log(res)
                showOrderToModalDetail(res)
            },
            error: function (res) {

            },
        })
    }
    function showOrderToModalDetail(paramOrder) {
        $("#inp-detail-orderId").val(paramOrder.orderId)
        $("#inp-detail-combo").val(paramOrder.kichCo)
        $("#inp-detail-duong-kinh").val(paramOrder.duongKinh)
        $("#inp-detail-suon-nuong").val(paramOrder.suon)
        $("#inp-detail-drink").val(paramOrder.idLoaiNuocUong)
        $("#inp-detail-number-drink").val(paramOrder.soLuongNuoc)
        $("#inp-detail-voucherid").val(paramOrder.idVourcher)
        $("#inp-detail-pizza").val(paramOrder.loaiPizza)
        $("#inp-detail-salad").val(paramOrder.salad)
        $("#inp-detail-prices").val(paramOrder.thanhTien)
        $("#inp-detail-giam-gia").val(paramOrder.giamGia)
        $("#inp-detail-fullname").val(paramOrder.hoTen)
        $("#inp-detail-email").val(paramOrder.email)
        $("#inp-detail-mobile").val(paramOrder.soDienThoai)
        $("#inp-detail-address").val(paramOrder.diaChi)
        $("#inp-detail-message").val(paramOrder.loiNhan)
        $("#select-detail-status").val(paramOrder.trangThai)
        $("#inp-detail-create-order").val(paramOrder.ngayTao)
        $("#inp-detail-update").val(paramOrder.ngayCapNhat)
        $("#modal-detail").modal("show")
    }
    function getDataFromModal(paramOrder){
        var discount = getDiscount($("#inp-insert-voucherid").val())
        var vDonGia = $("#inp-insert-prices").val()
        var vThanhTien = vDonGia - vDonGia*discount/100
        paramOrder.kichCo=$("#select-insert-combo option:selected").val()
        paramOrder.duongKinh=$("#inp-insert-duong-kinh").val().trim()
        paramOrder.suon=$("#inp-insert-suon-nuong").val().trim()
        paramOrder.idLoaiNuocUong=$("#select-insert-drink option:selected").val()
        paramOrder.soLuongNuoc=$("#inp-insert-number-drink").val().trim()
        paramOrder.idVourcher=$("#inp-insert-voucherid").val().trim()
        paramOrder.loaiPizza=$("#select-insert-pizza option:selected").val()
        paramOrder.salad=$("#inp-insert-salad").val().trim()
        paramOrder.thanhTien = vThanhTien
        //paramOrder.thanhTien=$("#inp-insert-prices").val().trim()
        paramOrder.hoTen=$("#inp-insert-fullname").val().trim()
        paramOrder.email=$("#inp-insert-email").val().trim()
        paramOrder.soDienThoai=$("#inp-insert-mobile").val().trim()
        paramOrder.diaChi=$("#inp-insert-address").val().trim()
        paramOrder.loiNhan=$("#inp-insert-message").val().trim()
    }
    function checkValid(paramOrder) {
        var result = true
        for (var prop in paramOrder) {
            if ( (prop != "email" && prop!="loiNhan" && prop!="idVourcher") && paramOrder[prop] == "" )  {
               
                result = false
                //alert(prop + " is null")
                toastr.warning(" Chưa nhập "+ prop  )
            }
            if (prop == "email" && paramOrder[prop] != ""){
                result = validateEmail(paramOrder[prop])
            }
        }
        
        return result
    }
    function updateOrder(paramStatus) {
        console.log(paramStatus)
        $.ajax({
            headers: {
                'Content-Type': 'application/json;charset=UTF-8' 
            },
            url: baseUrl + `/${gID}`,
            type: "PUT",
            data: JSON.stringify(paramStatus),
            success: function (res) {
                console.log(res)
                console.log("aa")
                $("#modal-detail").modal("hide")
                getAllOrdersAPI()
                toastr.success('Đã update trạng thái thành công')
            },
            error: function (res) {
            }
        })
    }
    function getDrink(){
        $.ajax({
            url : "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            success: function (res) {
                for (var drink of res ){
                    $("#select-insert-drink").append($("<option>",{val: drink.maNuocUong,text:drink.tenNuocUong }))
                }
            },
            error: function (res) {
            },
        })
    }
    function getDiscount(paramVoucherID){
        var vGiamGia = 0
        if (paramVoucherID !=""){
            $.ajax({
                url:"http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/"+paramVoucherID,
                type:"GET",
                async:false,
                success: function(res){
                    vGiamGia= res.phanTramGiamGia
                },
                error: function(res){          
                }
            })
        }
       return vGiamGia
    }
    function validateEmail (emailAdress){
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (emailAdress.match(regexEmail)) {
            return true; 
        } else {
            toastr.warning("Địa chỉ email không chính xác")
            return false; 
        }
    }
});
